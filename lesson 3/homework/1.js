/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
var currentPosition = 0;
var mySlider = document.getElementById('slider');
var mySliderControls = document.getElementById('SliderControls');
var mySliderControlsButton = mySliderControls.querySelectorAll('button');
var myImg;
function load() {
  var addImg = document.createElement('img');
  addImg.setAttribute('src', OurSliderImages[currentPosition]);
  mySlider.appendChild(addImg);
  setTimeout(opacit, 0);  ///////>> - присваивание стилей для анимации. без таймаута не работает? решение покрасивее?
  // присваивание класса без таймаута тоже не запускает анимацию.
  // opacit();
  myImg = mySlider.querySelector('img');
}
function opacit() {
  // myImg.style.transitionDuration = '1s';
  // myImg.style.opacity = '0';
  // myImg.style.opacity = '1';
  myImg.classList.add('loadedImage');
}
window.onload = load(); //1
function RenderImage(e) { //2
  myImg.remove();
  if (e.target.id === 'NextSlide') {
    NextSlide();
  }
  if (e.target.id === 'PrevSlide') {
    PrevSlide();
  }
  load();
}
function NextSlide() { //3
  currentPosition = +currentPosition + 1;
  if (currentPosition == +OurSliderImages.length) {
    currentPosition = 0;
  }
}
function PrevSlide() { //4
  currentPosition = +currentPosition - 1;
  if (currentPosition < 0) {
    currentPosition = +OurSliderImages.length - 1;
  }
}
mySliderControlsButton.forEach(function (elem) {
  elem.addEventListener('click', RenderImage);
});