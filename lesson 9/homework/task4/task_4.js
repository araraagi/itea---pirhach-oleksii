/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import nest from './modules/nest.js';
import fly from './modules/fly.js';
import swim from './modules/swim.js';
import eat from './modules/eat.js';
import hunt from './modules/hunt.js';
import sing from './modules/sing.js';
import mail from './modules/mail.js';
import run from './modules/run.js';

document.addEventListener('DOMContentLoaded', () => {

  function Bird(name) {
    this.name = name;
    this.nest = nest;
    this.fly = fly;
    this.swim = swim;
    this.eat = eat;
    this.hunt = hunt;
    this.sing = sing;
    this.mail = mail;
    this.run = run;
    }

  let bird1 = new Bird('Папуг');
  let bird2 = new Bird('Голубь');

  console.log(bird1);
  bird1.fly();
  console.log(bird2);
  bird1.mail();

});