/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.co44m/api/json/get/cgCRXqNTtu?indent=2 ---???

*/
document.addEventListener('DOMContentLoaded', () => {
    let renderHere = document.getElementById('renderHere');
    let addPostButton = document.getElementById('addPost');
    class Posts {
        constructor(isActive, title, about) {
            this._id = +new Date();
            this.isActive = isActive;
            this.title = title;
            this.about = about;
            this.likes = 0;
            let d = new Date();
            this.created_at = d.toLocaleString();
        }

        addLike() {
            return (e) => {
                this.likes = this.likes + 1;
                localStorage.setItem('postsArray', JSON.stringify(arrayFromLocal));
                console.log(this._id + ' addLike ' + this.likes);
            }
        }

        render(target) {
            let node;
            node = document.createElement('div');
            node.className = 'Posts';
            node.dataset.id = this.id;
            node.innerHTML = `
                <li id="${this._id}">
                    <div class="message__id">
                        ${this._id}
                    </div>
                    <div class="message__date">
                        ${this.created_at}
                    </div>
                    <div class="message__status">
                        Status: ${this.isActive}
                    </div>
                    <div class="message__title">
                        <b>${this.title}</b>
                    </div>
                    <div class="message__about">
                        ${this.about}
                    </div>
                    <div class="message__likes">
                        likes: ${this.likes}
                    </div>
                    <div class="post__controls">
                        <form>
                        <input type="submit" class="_answerMessage" value="Add like">
                        </form>
                    </div>
                </li>
            `;
            node.querySelector('._answerMessage').addEventListener('click', this.addLike());
            target.appendChild(node);
        }
    };

    let arrayFromLocal;
    let reRender = () => {
        arrayFromLocal = JSON.parse(localStorage.getItem('postsArray'));
        if (arrayFromLocal === null) { arrayFromLocal = [] };
        arrayFromLocal.forEach((elem, i) => {
            let x = new Posts;
            x._id = elem._id;
            x.isActive = elem.isActive;
            x.title = elem.title;
            x.about = elem.about;
            x.likes = elem.likes;
            x.created_at = elem.created_at;
            arrayFromLocal[i] = x;
        });
        arrayFromLocal.map(Posts => Posts.render(renderHere));
    }
    reRender();

    let addPost = () => {
        let post = new Posts('active', 'My post', 'About my post');
        arrayFromLocal.push(post);
        localStorage.setItem('postsArray', JSON.stringify(arrayFromLocal));
    };
    addPostButton.addEventListener('click', addPost);

    console.log('для экспорта?: ' + JSON.stringify(arrayFromLocal));
});