/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/
document.addEventListener('DOMContentLoaded', () => {
    let loginArea = document.getElementById('loginArea');
    let pas1 = document.getElementById('pas1');
    let loginForm = document.getElementById('loginForm');
    let helloArea = document.getElementById('helloArea');
    let submitButton = document.getElementById('submitButton');
    let deleteUser = document.getElementById('deleteUser');
    ////
    let obj = {
        name: 'admin',
        password: '123'
    };
    localStorage.setItem('allowedUser', JSON.stringify(obj));
    ////
    let user = localStorage.getItem('user');
    let deleteObj = () => {
        localStorage.removeItem('user');
    };
    let load = (e) => {
        if (user === null) {
            loginForm.style.display = 'block';
            helloArea.style.display = 'none';
        } else {
            loginForm.style.display = 'none';
            helloArea.style.display = 'block';
            let obj = JSON.parse(localStorage.getItem("user"));
            helloArea.querySelector('span').innerText = `Привет, ${obj.name}!`;
        };
    }
    load();
    let login = () => {
        let obj = {
            name: loginArea.value,
            password: pas1.value
        };
        let allowedUser = localStorage.getItem('allowedUser');
        let user = JSON.stringify(obj);
        if (user === allowedUser) {
            localStorage.setItem('user', user);
        }
    };
    // console.log(window.localStorage);
    deleteUser.addEventListener('click', deleteObj);
    submitButton.addEventListener('click', login);
});