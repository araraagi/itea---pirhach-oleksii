/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
document.addEventListener('DOMContentLoaded', () => {

    let colorChangeButton = document.getElementById('colorChangeButton');
    let backColor = localStorage.getItem('backColor');
    document.body.style.background = backColor;

    let getRandomNum = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return ('0' + (Math.floor(Math.random() * (max - min + 1)) + min).toString(16).toUpperCase()).slice(-2);
    }

    let changeColor = () => {
        let randomColor = ('#' + getRandomNum(0, 256) + getRandomNum(0, 256) + getRandomNum(0, 256));
        localStorage.setItem('backColor', randomColor);
        document.body.style.background = randomColor;
        console.log(window.localStorage);
    }
    colorChangeButton.addEventListener('click', changeColor);

});