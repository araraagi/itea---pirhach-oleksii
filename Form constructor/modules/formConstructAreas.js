const formConstructAreas = {
    TextInput: `
<form class="form" id="inputText">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Placeholder</span>
        <input type="text" id="placeholder" name="placeholder" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    TextAreaInput: `
<form class="form" id="inputTextArea">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Placeholder</span>
        <input type="text" id="placeholder" name="placeholder" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
    <span class="form__text">Ширина в символах</span>
    <input type="number" id="cols" name="cols" class="form__input">
    </label><br>
    
    <label>
    <span class="form__text">Высота в страках</span>
    <input type="number" id="rows" name="rows" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    CheckBoxInput: `
<form class="form" id="inputCheckBox">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>

    <label>
        <span class="form__text">Checked</span>
        <input type="checkbox" id="checked" name="checked" value=true>
    </label><br>
</form>
`,
    RadioInput: `
<form class="form" id="inputRadio">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>

    <label>
        <span class="form__text">Checked</span>
        <input type="checkbox" id="checked" name="checked" value=true>
    </label><br>
</form>
`,
    DateInput: `
<form class="form" id="inputDate">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
        
    <label>
        <span class="form__text">Value</span>
        <input type="date" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Минимальная дата</span>
        <input type="date" id="minDate" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Максимальная дата</span>
        <input type="date" id="maxDate" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Шаг (дней)</span>
        <input type="number" id="step" name="step" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    DateTimeLocalInput: `
    <form class="form" id="inputDateTimeLocal">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
        
    <label>
        <span class="form__text">Value</span>
        <input type="datetime-local" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Минимальная дата</span>
        <input type="datetime-local" id="minDate" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Максимальная дата</span>
        <input type="datetime-local" id="maxDate" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Шаг (дней)</span>
        <input type="number" id="step" name="step" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    ColorInput: `
    <form class="form" id="inputColor">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
        
    <label>
        <span class="form__text">Value</span>
        <input type="color" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    PasswordInput: `
    <form class="form" id="inputPassword">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Placeholder</span>
        <input type="text" id="placeholder" name="placeholder" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Мин. симв.</span>
        <input type="number" id="minPass" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Макс. симв.</span>
        <input type="number" id="maxPass" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    EmailInput: `
    <form class="form" id="inputEmail">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
        <label>
            <span class="form__text">Placeholder</span>
            <input type="text" id="placeholder" name="placeholder" class="form__input">
        </label><br>

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
    <span class="form__text">Required</span>
    <input type="checkbox" id="required" name="required" value='required'>
</label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    RangeInput: `
    <form class="form" id="inputRange">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Value</span>
        <input type="number" id="value" name="value" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Min</span>
        <input type="number" id="minPass" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Max</span>
        <input type="number" id="maxPass" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Step</span>
        <input type="number" id="step" name="step" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    FileInput: `
    <form class="form" id="inputFile">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Multiple</span>
        <input type="checkbox" id="multiple" name="multiple" value=true>
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    NumberInput: `
    <form class="form" id="inputNumber">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Value</span>
        <input type="number" id="value" name="value" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Min</span>
        <input type="number" id="minPass" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Max</span>
        <input type="number" id="maxPass" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Step</span>
        <input type="number" id="step" name="step" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    TimeInput: `
    <form class="form" id="inputTime">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Value</span>
        <input type="time" id="value" name="value" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Min</span>
        <input type="time" id="minPass" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Max</span>
        <input type="time" id="maxPass" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Step (seconds)</span>
        <input type="number" id="step" name="step" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    UrlInput: `
    <form class="form" id="inputUrl">

    <label>
        <span class="form__text">Name</span>
        <input type="text" id="name" name="name" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Label</span>
        <input type="text" name="label" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Placeholder</span>
        <input type="text" id="placeholder" name="placeholder" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">Value</span>
        <input type="url" id="value" name="value" class="form__input">
    </label><br>
    
    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Мин. симв.</span>
        <input type="number" id="minPass" name="min" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Макс. симв.</span>
        <input type="number" id="maxPass" name="max" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Required</span>
        <input type="checkbox" id="required" name="required" value='required'>
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`,
    SubmitInput: `
    <form class="form" id="inputSubmit">

    <label>
        <span class="form__text">Value</span>
        <input type="text" id="value" name="value" class="form__input">
    </label><br>

    <label>
        <span class="form__text">ClassName</span>
        <input type="text" id="className" name="className" class="form__input">
    </label><br>

    <label>
        <span class="form__text">Disabled</span>
        <input type="checkbox" id="disabled" name="disabled" value='disabled'>
    </label><br>
</form>
`
    ///
}
export default formConstructAreas;