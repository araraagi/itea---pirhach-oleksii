// export class Storage {
//     constructor({ action }) {
//         let myFormInputs = [];
//         this.myFormInputs = myFormInputs;
//     }
// };

// let myFormInputs = Storage.myFormInputs;
///////////////////////////////////////////////
export class Form {
    constructor({ action, name, autocomplete, method, enctype }) {
        this.action = action;
        this.name = name;
        this.autocomplete = autocomplete;
        this.method = method;
        this.enctype = enctype;
    }
    render(target) {
        let node;
        node = document.createElement('form');
        node.innerHTML = `
            
        `;
        node.action = this.action;
        node.name = this.name;
        node.autocomplete = this.autocomplete;
        node.method = this.method;
        node.enctype = this.enctype;
        target.appendChild(node);
    }
};

export class InputElement {
    constructor(id, name, className, disabled, value) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.className = className;
        this.disabled = disabled;
    }
    reRender() {
        addForm(renderHere);
        myFormInputs.map(elem => elem.render(renderHere.querySelector('form')));
        localStorage.setItem('formInputs', JSON.stringify(myFormInputs));
    }
    deleteInput() {
        return (e) => {
            e.preventDefault();
            console.log(this.id);
            let z = myFormInputs.findIndex(x => x.id === this.id);
            if (z > -1) { myFormInputs.splice(z, 1) };
            this.reRender();
        }
    }
    upInput() {
        return (e) => {
            e.preventDefault();
            let z = myFormInputs.findIndex(x => x.id === this.id);
            if (z > 0) {
                [myFormInputs[z], myFormInputs[z - 1]] = [myFormInputs[z - 1], myFormInputs[z]]
            };
            this.reRender();
        }
    }
    downInput() {
        return (e) => {
            e.preventDefault();
            let z = myFormInputs.findIndex(x => x.id === this.id);
            if (z < myFormInputs.length - 1) {
                [myFormInputs[z], myFormInputs[z + 1]] = [myFormInputs[z + 1], myFormInputs[z]]
            };
            this.reRender();
        }
    }

    editInput() {
        return (e) => {
            e.preventDefault();
            let z = myFormInputs.findIndex(x => x.id === this.id);
            if (z > -1) {
                let editedInput = myFormInputs[z];
                formConstructArea.innerHTML = formConstructAreas[editedInput.classN];
                //classes visual
                let inputForm = formConstructArea.querySelector('form');
                let inputFormElems = Array.from(inputForm.elements);
                inputFormElems.forEach((elem) => {
                    if (editedInput[elem.name]) {
                        if (elem.type === 'checkbox') {
                            elem.checked = true;
                        } else {
                            elem.value = editedInput[elem.name];
                        }
                    }
                });
                console.log(editedInput);

                let saveEditInput = () => {
                    inputFormElems.forEach((elem) => {
                        if (elem.type === 'checkbox') {
                            if (elem.checked) {
                                editedInput[elem.name] = elem.value;
                            } else {
                                editedInput[elem.name] = false;
                            };
                        }
                        else {
                            if (elem.value) {
                                editedInput[elem.name] = elem.value;
                            } else {
                                editedInput[elem.name] = false;
                            };
                        }
                    });
                    console.log(editedInput);
                    this.reRender();
                };

                let submitEdit = document.createElement('button');
                submitEdit.innerText = 'Сохранить изменения';
                submitEdit.addEventListener('click', saveEditInput);
                formConstructArea.appendChild(submitEdit);

            };
            // this.reRender();
        }
    }
};

export class TextInput extends InputElement {
    constructor({ id, name, label, placeholder, value, className, required, disabled }) {
        super(id, name, className, disabled, value);
        this.classN = 'TextInput';
        this.type = 'text';
        this.label = label;
        this.placeholder = placeholder;
        if (required) {this.required = 'required'} else {this.required = false};
    }
    render(target) {
        let node;
        node = document.createElement('label');
        node.id = this.id;
        node.innerHTML = `
            <span>${this.label}</span>
            <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
            <br>
        `;
        let input = node.querySelector('input');
        if (this.placeholder) { input.placeholder = this.placeholder };
        if (this.value) { input.value = this.value };
        if (this.className) { input.classList.add(this.className) };
        if (this.required) { input.required = 'required' };
        if (this.disabled) { input.disabled = true };
        node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
        node.querySelector('._upInput').addEventListener('click', this.upInput());
        node.querySelector('._downInput').addEventListener('click', this.downInput());
        node.querySelector('._editInput').addEventListener('click', this.editInput());
        target.appendChild(node);
    }
};
