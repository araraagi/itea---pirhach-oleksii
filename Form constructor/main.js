import formConstructAreas from './modules/formConstructAreas';
// import { Storage } from './modules/classes';
// import { Form, InputElement, TextInput } from './modules/classes';

document.addEventListener('DOMContentLoaded', () => {
    const renderHere = document.getElementById('renderHere');
    const codeHere = document.getElementById('codeHere');
    const formConstructArea = document.getElementById('formConstructArea');
    const constructButtonsForm = document.getElementById('constructButtonsForm');
    const settingsForm = document.getElementById('settingsForm');
    //////////////////////////////////////////////////////////////
    // let myFormInputs = Storage.myFormInputs;
    let myFormInputs = [];

    //classes start

    class Form {
        constructor({ action, name, autocomplete, method, enctype }) {
            this.action = action;
            this.name = name;
            this.autocomplete = autocomplete;
            this.method = method;
            this.enctype = enctype;
        }
        render(target) {
            let node;
            node = document.createElement('form');
            node.innerHTML = `
                
            `;
            node.action = this.action;
            node.name = this.name;
            node.autocomplete = this.autocomplete;
            node.method = this.method;
            node.enctype = this.enctype;
            target.appendChild(node);
        }
    };

    class InputElement {
        constructor(id, name, className, disabled, value) {
            this.id = id;
            this.name = name;
            this.value = value;
            this.className = className;
            this.disabled = disabled;
        }
        reRender() {
            addForm(renderHere);
            myFormInputs.map(elem => elem.render(renderHere.querySelector('form')));
            localStorage.setItem('formInputs', JSON.stringify(myFormInputs));
        }
        deleteInput() {
            return (e) => {
                e.preventDefault();
                console.log(this.id);
                let z = myFormInputs.findIndex(x => x.id === this.id);
                if (z > -1) { myFormInputs.splice(z, 1) };
                this.reRender();
            }
        }
        upInput() {
            return (e) => {
                e.preventDefault();
                let z = myFormInputs.findIndex(x => x.id === this.id);
                if (z > 0) {
                    [myFormInputs[z], myFormInputs[z - 1]] = [myFormInputs[z - 1], myFormInputs[z]]
                };
                this.reRender();
            }
        }
        downInput() {
            return (e) => {
                e.preventDefault();
                let z = myFormInputs.findIndex(x => x.id === this.id);
                if (z < myFormInputs.length - 1) {
                    [myFormInputs[z], myFormInputs[z + 1]] = [myFormInputs[z + 1], myFormInputs[z]]
                };
                this.reRender();
            }
        }

        editInput() {
            return (e) => {
                e.preventDefault();
                let z = myFormInputs.findIndex(x => x.id === this.id);
                if (z > -1) {
                    let editedInput = myFormInputs[z];
                    formConstructArea.innerHTML = formConstructAreas[editedInput.classN];
                    //classes visual
                    let inputForm = formConstructArea.querySelector('form');
                    let inputFormElems = Array.from(inputForm.elements);
                    inputFormElems.forEach((elem) => {
                        if (editedInput[elem.name]) {
                            if (elem.type === 'checkbox') {
                                elem.checked = true;
                            } else {
                                elem.value = editedInput[elem.name];
                            }
                        }
                    });
                    console.log(editedInput);

                    let saveEditInput = () => {
                        inputFormElems.forEach((elem) => {
                            if (elem.type === 'checkbox') {
                                if (elem.checked) {
                                    editedInput[elem.name] = elem.value;
                                } else {
                                    editedInput[elem.name] = false;
                                };
                            }
                            else {
                                if (elem.value) {
                                    editedInput[elem.name] = elem.value;
                                } else {
                                    editedInput[elem.name] = false;
                                };
                            }
                        });
                        console.log(editedInput);
                        this.reRender();
                    };

                    let submitEdit = document.createElement('button');
                    submitEdit.innerText = 'Сохранить изменения';
                    submitEdit.addEventListener('click', saveEditInput);
                    formConstructArea.appendChild(submitEdit);

                };
                // this.reRender();
            }
        }
    };

    class TextInput extends InputElement {
        constructor({ id, name, label, placeholder, value, className, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'TextInput';
            this.type = 'text';
            this.label = label;
            this.placeholder = placeholder;
            if (required) {this.required = 'required'} else {this.required = false};//всеравно при рендере задает булев
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.placeholder) { input.placeholder = this.placeholder };
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.required) { input.required = 'required'};
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class TextAreaInput extends InputElement {
        constructor({ id, name, label, value, placeholder, className, required, disabled, cols, rows }) {
            super(id, name, className, disabled, value);
            this.classN = 'TextAreaInput';
            this.type = 'textarea';
            this.label = label;
            this.placeholder = placeholder;
            this.required = required;
            this.cols = cols;
            this.rows = rows;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <textarea name="${this.name}"></textarea></textarea><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('textarea');
            if (this.placeholder) { input.placeholder = this.placeholder };
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            if (this.cols) { input.cols = this.cols };
            if (this.rows) { input.rows = this.rows };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class CheckBoxInput extends InputElement {
        constructor({ id, name, label, className, disabled, value, checked }) {
            super(id, name, className, disabled, value);
            this.classN = 'CheckBoxInput';
            this.type = 'checkbox';
            this.label = label;
            this.checked = checked;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            if (this.checked) { input.checked = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class RadioInput extends InputElement {
        constructor({ id, name, label, className, disabled, value, checked }) {
            super(id, name, className, disabled, value);
            this.classN = 'RadioInput';
            this.type = 'radio';
            this.label = label;
            this.checked = checked;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            if (this.checked) { input.checked = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class DateInput extends InputElement {
        constructor({ id, name, label, className, value, min, max, step, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'DateInput';
            this.type = 'date';
            this.label = label;
            this.min = min;
            this.max = max;
            this.step = step;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.step) { input.step = this.step };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class DateTimeLocalInput extends InputElement {
        constructor({ id, name, label, className, value, min, max, step, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'DateTimeLocalInput';
            this.type = 'datetime-local';
            this.label = label;
            this.min = min;
            this.max = max;
            this.step = step;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.step) { input.step = this.step };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class ColorInput extends InputElement {
        constructor({ id, name, label, className, value, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'ColorInput';
            this.type = 'color';
            this.label = label;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class PasswordInput extends InputElement {
        constructor({ id, name, label, className, placeholder, value, min, max, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'PasswordInput';
            this.type = 'password';
            this.label = label;
            this.placeholder = placeholder;
            this.min = min;
            this.max = max;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.placeholder) { input.placeholder = this.placeholder };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class EmailInput extends InputElement {
        constructor({ id, name, label, placeholder, value, className, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'EmailInput';
            this.type = 'email';
            this.label = label;
            this.placeholder = placeholder;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.placeholder) { input.placeholder = this.placeholder };
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class RangeInput extends InputElement {
        constructor({ id, name, label, className, value, min, max, step, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'RangeInput';
            this.type = 'range';
            this.label = label;
            this.min = min;
            this.max = max;
            this.step = step;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.step) { input.step = this.step };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class FileInput extends InputElement {
        constructor({ id, name, label, className, value, multiple, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'FileInput';
            this.type = 'file';
            this.label = label;
            this.multiple = multiple;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.multiple) { input.multiple = true };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class NumberInput extends InputElement {
        constructor({ id, name, label, className, value, min, max, step, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'NumberInput';
            this.type = 'number';
            this.label = label;
            this.min = min;
            this.max = max;
            this.step = step;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.step) { input.step = this.step };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class TimeInput extends InputElement {
        constructor({ id, name, label, className, value, min, max, step, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'TimeInput';
            this.type = 'time';
            this.label = label;
            this.min = min;
            this.max = max;
            this.step = step;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.step) { input.step = this.step };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class UrlInput extends InputElement {
        constructor({ id, name, label, className, placeholder, value, min, max, required, disabled }) {
            super(id, name, className, disabled, value);
            this.classN = 'UrlInput';
            this.type = 'url';
            this.label = label;
            this.placeholder = placeholder;
            this.min = min;
            this.max = max;
            this.required = required;
        }
        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <span>${this.label}</span>
                <input type="${this.type}" name="${this.name}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.className) { input.classList.add(this.className) };
            if (this.placeholder) { input.placeholder = this.placeholder };
            if (this.value) { input.value = this.value };
            if (this.min) { input.min = this.min };
            if (this.max) { input.max = this.max };
            if (this.required) { input.required = 'required' };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
        }
    };

    class SubmitInput extends InputElement {
        constructor({ id, name, className, disabled, value }) {
            super(id, name, className, disabled, value);
            this.classN = 'SubmitInput';
            this.type = 'submit';
        }

        resultObj() { //вывод значений формы через сабмит
            let submits = renderHere.querySelectorAll('[type="submit"]');
            if (Array.from(submits).length === 1) {
                Array.from(submits)[0].addEventListener('click', (e) => {
                    e.preventDefault();
                    let resultObj = {};
                    Array.from(renderHere.querySelector('form').elements).forEach((elem) => {
                        if (elem.type === 'checkbox') { //вывод значения только отмеченных чекбоксов
                            if (elem.checked) {
                                resultObj[elem.name] = elem.value;
                            }
                        } else {
                            if (resultObj[elem.name]) { //вывод значения отмеченной радиокнопки
                                if (elem.checked) {
                                    resultObj[elem.name] = elem.value;
                                }
                            } else {
                                resultObj[elem.name] = elem.value;
                            };
                        };
                    });
                    console.log(resultObj);
                });
            };
        }

        render(target) {
            let node;
            node = document.createElement('label');
            node.id = this.id;
            node.innerHTML = `
                <input type="${this.type}"><button class='_upInput'>UP</button><button class='_downInput'>DOWN</button><button class='_deleteInput'>DEL</button><button class='_editInput'>EDIT</button>
                <br>
            `;
            let input = node.querySelector('input');
            if (this.value) { input.value = this.value };
            if (this.className) { input.classList.add(this.className) };
            if (this.disabled) { input.disabled = true };
            node.querySelector('._deleteInput').addEventListener('click', this.deleteInput());
            node.querySelector('._upInput').addEventListener('click', this.upInput());
            node.querySelector('._downInput').addEventListener('click', this.downInput());
            node.querySelector('._editInput').addEventListener('click', this.editInput());
            target.appendChild(node);
            this.resultObj();
        }
    };
    //classes end

    ////initFormSettings
    let parsedForm;

    if (localStorage.getItem('formSettings')) {
        let parsedFormSettings = JSON.parse(localStorage.getItem('formSettings'));
        settingsForm.elements.action.value = parsedFormSettings.action;
        settingsForm.elements.formName.value = parsedFormSettings.name;
        settingsForm.elements.autocomplete.value = parsedFormSettings.autocomplete;
        settingsForm.elements.method.value = parsedFormSettings.method;
        settingsForm.elements.enctype.value = parsedFormSettings.enctype;
        parsedForm = new Form(parsedFormSettings);
    };
    ////

    ///////////////////////ClassesFactory-start
    class InputFactory {
        makeInput(input) {
            let InputClass = null;
            if (input.classN === 'TextInput') {
                InputClass = TextInput;
            } else if (input.classN === 'TextAreaInput') {
                InputClass = TextAreaInput;
            } else if (input.classN === 'CheckBoxInput') {
                InputClass = CheckBoxInput;
            } else if (input.classN === 'RadioInput') {
                InputClass = RadioInput;
            } else if (input.classN === 'DateInput') {
                InputClass = DateInput;
            } else if (input.classN === 'DateTimeLocalInput') {
                InputClass = DateTimeLocalInput;
            } else if (input.classN === 'ColorInput') {
                InputClass = ColorInput;
            } else if (input.classN === 'PasswordInput') {
                InputClass = PasswordInput;
            } else if (input.classN === 'EmailInput') {
                InputClass = EmailInput;
            } else if (input.classN === 'RangeInput') {
                InputClass = RangeInput;
            } else if (input.classN === 'FileInput') {
                InputClass = FileInput;
            } else if (input.classN === 'NumberInput') {
                InputClass = NumberInput;
            } else if (input.classN === 'TimeInput') {
                InputClass = TimeInput;
            } else if (input.classN === 'UrlInput') {
                InputClass = UrlInput;
            } else if (input.classN === 'SubmitInput') {
                InputClass = SubmitInput;
            } else {
                console.error('wrong classN');
                return false;
            }
            return new InputClass(input);
        }
    }
    const mySuperForge = new InputFactory();
    //initPage
    if (localStorage.getItem('formInputs')) {
        myFormInputs = [];
        let parsedInputs = JSON.parse(localStorage.getItem('formInputs'));
        console.log(parsedInputs);
        parsedInputs.forEach((elem, i) => {
            myFormInputs[i] = mySuperForge.makeInput(elem);
        });
        console.log(myFormInputs);
        parsedForm.render(renderHere);
        myFormInputs.map(elem => elem.render(renderHere.querySelector('form')));
    };
    ///////////////////////ClassesFactory-finish


    //pressedButton
    let selectedButton = 'TextInput';
    formConstructArea.innerHTML = formConstructAreas[selectedButton];

    constructButtonsForm.addEventListener('change', () => {
        selectedButton = constructButtonsForm.elements.formConstructAreaButton.value;
        formConstructArea.innerHTML = formConstructAreas[selectedButton];
    });

    //addInputs
    let refreshInputs = (target) => {
        myFormInputs.map(elem => elem.render(target.querySelector('form')));
        localStorage.setItem('formInputs', JSON.stringify(myFormInputs));
    }

    let addForm = (target) => {
        let input = document.getElementById('settingsForm');
        let action = input.elements.action.value;
        let name = input.elements.formName.value;
        let autocomplete = input.elements.autocomplete.value;
        let method = input.elements.method.value;
        let enctype = input.elements.enctype.value;
        let myForm = new Form({ action, name, autocomplete, method, enctype });
        renderHere.innerHTML = null;
        myForm.render(target);
        localStorage.setItem('formSettings', JSON.stringify(myForm));
    };
    /////////inputFuncs start
    const addTextInput = (target) => {
        let input = document.getElementById('inputText');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let placeholder = input.elements.placeholder.value;
        let value = input.elements.value.value;
        let className = input.elements.className.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new TextInput({ id, name, label, placeholder, value, className, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addTextAreaInput = (target) => {
        let input = document.getElementById('inputTextArea');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let placeholder = input.elements.placeholder.value;
        let value = input.elements.value.value;
        let className = input.elements.className.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let cols = input.elements.cols.value;
        let rows = input.elements.rows.value;
        let myInput = new TextAreaInput({ id, name, label, placeholder, value, className, required, disabled, cols, rows });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addCheckBoxInput = (target) => {
        let input = document.getElementById('inputCheckBox');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let disabled = input.elements.disabled.checked;
        let value = input.elements.value.value;
        let checked = input.elements.checked.checked;
        let myInput = new CheckBoxInput({ id, name, label, className, disabled, value, checked });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addRadioInput = (target) => {
        let input = document.getElementById('inputRadio');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let disabled = input.elements.disabled.checked;
        let value = input.elements.value.value;
        let checked = input.elements.checked.checked;
        let myInput = new RadioInput({ id, name, label, className, disabled, value, checked });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addDateInput = (target) => {
        let input = document.getElementById('inputDate');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let step = input.elements.step.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new DateInput({ id, name, label, className, value, min, max, step, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addDateTimeLocalInput = (target) => {
        let input = document.getElementById('inputDateTimeLocal');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let step = input.elements.step.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new DateTimeLocalInput({ id, name, label, className, value, min, max, step, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addColorInput = (target) => {
        let input = document.getElementById('inputColor');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new ColorInput({ id, name, label, className, value, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addPasswordInput = (target) => {
        let input = document.getElementById('inputPassword');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let placeholder = input.elements.placeholder.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new PasswordInput({ id, name, label, className, placeholder, value, min, max, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addEmailInput = (target) => {
        let input = document.getElementById('inputEmail');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let placeholder = input.elements.placeholder.value;
        let value = input.elements.value.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new EmailInput({ id, name, label, placeholder, value, className, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addRangeInput = (target) => {
        let input = document.getElementById('inputRange');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let step = input.elements.step.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new RangeInput({ id, name, label, className, value, min, max, step, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };

    const addFileInput = (target) => {
        let input = document.getElementById('inputFile');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = '';
        let multiple = input.elements.multiple.checked;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new FileInput({ id, name, label, className, value, multiple, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };
    const addNumberInput = (target) => {
        let input = document.getElementById('inputNumber');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let step = input.elements.step.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new NumberInput({ id, name, label, className, value, min, max, step, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };
    const addTimeInput = (target) => {
        let input = document.getElementById('inputTime');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let step = input.elements.step.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new TimeInput({ id, name, label, className, value, min, max, step, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };
    const addUrlInput = (target) => {
        let input = document.getElementById('inputUrl');
        let id = +new Date();
        let name = input.elements.name.value;
        let label = input.elements.label.value;
        let className = input.elements.className.value;
        let placeholder = input.elements.placeholder.value;
        let value = input.elements.value.value;
        let min = input.elements.min.value;
        let max = input.elements.max.value;
        let required = input.elements.required.checked;
        let disabled = input.elements.disabled.checked;
        let myInput = new UrlInput({ id, name, label, className, placeholder, value, min, max, required, disabled });
        myFormInputs.push(myInput);
        refreshInputs(target);
    };
    const addSubmitInput = (target) => {
        let input = document.getElementById('inputSubmit');
        let id = +new Date();
        let name = '';
        let className = input.elements.className.value;
        let value = input.elements.value.value;
        let disabled = input.elements.disabled.checked;
        let myInput = new SubmitInput({ id, name, className, disabled, value });
        myFormInputs.push(myInput);
        refreshInputs(target);
        console.log('function to first submit added');
    };
    /////////inputFuncs finish

    let addInput = (e) => {
        // e.preventDefault();
        addForm(renderHere);
        if (selectedButton === 'TextInput') { addTextInput(renderHere) };
        if (selectedButton === 'TextAreaInput') { addTextAreaInput(renderHere) };
        if (selectedButton === 'CheckBoxInput') { addCheckBoxInput(renderHere) };
        if (selectedButton === 'RadioInput') { addRadioInput(renderHere) };
        if (selectedButton === 'DateInput') { addDateInput(renderHere) };
        if (selectedButton === 'DateTimeLocalInput') { addDateTimeLocalInput(renderHere) };
        if (selectedButton === 'ColorInput') { addColorInput(renderHere) };
        if (selectedButton === 'PasswordInput') { addPasswordInput(renderHere) };
        if (selectedButton === 'EmailInput') { addEmailInput(renderHere) };
        if (selectedButton === 'RangeInput') { addRangeInput(renderHere) };
        if (selectedButton === 'FileInput') { addFileInput(renderHere) };
        if (selectedButton === 'NumberInput') { addNumberInput(renderHere) };
        if (selectedButton === 'TimeInput') { addTimeInput(renderHere) };
        if (selectedButton === 'UrlInput') { addUrlInput(renderHere) };
        if (selectedButton === 'SubmitInput') { addSubmitInput(renderHere) };
        console.log(myFormInputs);
    };

    let getCode = (e) => {
        // e.preventDefault();
        let result = renderHere.querySelectorAll('button');
        Array.from(result).forEach((elem) => {
            elem.remove();
        });
        let result2 = renderHere.querySelectorAll('label');
        Array.from(result2).forEach((elem) => {
            elem.removeAttribute('id');
        });
        let node = renderHere.innerHTML;
        codeHere.querySelector('code').innerText = node;
        addForm(renderHere);
        refreshInputs(renderHere);
    };
    let clearCode = (e) => {
        renderHere.innerHTML = null;
        localStorage.removeItem('formInputs');
    };

    document.getElementById('addInputButton').addEventListener('click', addInput);
    document.getElementById('getCodeButton').addEventListener('click', getCode);
    document.getElementById('clearCodeButton').addEventListener('click', clearCode);

});