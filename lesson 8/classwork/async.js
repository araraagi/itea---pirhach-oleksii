/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

async function getCompanies() {
  const companiesResponse = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const companies = await companiesResponse.json();
  let createTable = document.createElement('table');
  createTable.innerHTML = `
    <tr>
    <th>#</th>
    <th>Company</th>
    <th>Balance</th>
    <th>Показать дату регистрации</th>
    <th>Показать адрес</th>
    </tr>
  `;
  document.body.appendChild(createTable);
  companies.forEach((element, id) => {
    let createTr = document.createElement('tr');
    createTr.innerHTML = `
      <td>${id + 1}</td>
      <td>${element.company}</td>
      <td>${element.balance}</td>
      <td>
      <button mySpoiler>Показать</button>
      <span hidden>${element.registered}</span>
      </td>
      <td>
      <button mySpoiler>Показать</button>
      <span hidden>${JSON.stringify(element.address)}</span>
      </td>
    `;
    createTable.appendChild(createTr);
  });
  let removeAndShow = (e) => {
    let parentTd = e.target.closest('td');
    parentTd.querySelector('span').removeAttribute('hidden');
    e.target.remove();
  }
  let spoiler = document.querySelectorAll('[mySpoiler]');
  Array.from(spoiler).forEach((elem) => {
    elem.addEventListener('click', removeAndShow);
  })
}
getCompanies();