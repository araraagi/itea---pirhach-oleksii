
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
document.addEventListener('DOMContentLoaded', () => {
  let textInput = document.getElementById('textInput');
  let codeInput = document.getElementById('codeInput');
  let submButton = document.getElementById('subm');
  let decodeButton = document.getElementById('decode');
  submButton.addEventListener('click', (e) => {
    e.preventDefault();
    let newObject = {
      name: textInput.elements.name.value,
      age: textInput.elements.age.value,
      password: textInput.elements.password.value
    };
    newObject = JSON.stringify(newObject);
    console.log(newObject);
  })
  decodeButton.addEventListener('click', (e) => {
    e.preventDefault();
    let newCode = codeInput.elements.code.value;
    newCode = JSON.parse(newCode);
    console.log(newCode);
  })
})