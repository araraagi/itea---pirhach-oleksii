/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
document.addEventListener('DOMContentLoaded', () => {
  const encryptCesar = (shift, phrase) => {
    let phraseArray = phrase.split('');
    phraseArray.forEach((elem, i) => {
      phraseArray[i] = elem.charCodeAt() + shift;
    });
    return (String.fromCharCode(...phraseArray));
  };
  const decryptCesar = (shift, phrase) => {
    let phraseArray = phrase.split('');
    phraseArray.forEach((elem, i) => {
      phraseArray[i] = elem.charCodeAt() - shift;
    });
    return (String.fromCharCode(...phraseArray));
  };
  ///
  let x = encryptCesar(4, 'Hello!');
  console.log(x);
  let y = decryptCesar(4, x);
  console.log(y);
  ///
  const encryptCesar1 = encryptCesar.bind(null, 1);
  const decryptCesar1 = decryptCesar.bind(null, 1);
  let z = encryptCesar1('hello');
  console.log(z);
  let w = decryptCesar1(z);
  console.log(w);
  ///
});