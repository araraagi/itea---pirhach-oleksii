/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var train = {};
train.name = '043';
train.speed = 70;
train.passangeers = 40;
train.move = function() {
    console.log('Поезд ' + this.name + ' везет ' + this.passangeers + ' пассажиров со скоростью ' + this.speed + 'км/ч.');
}
train.stop = function() {
    this.speed = 0;
    console.log('Поезд ' + this.name + ' остановился. Скорость ' + this.speed);
}
train.takePass = function(x) {
    this.passangeers = this.passangeers + x;
    console.log('+ ' + x + ' пассажиров.')
}
console.log(train);
train.move();
console.log(train);
train.stop();
console.log(train);
train.takePass(5);
console.log(train);