/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
let colors = {
  background: 'purple',
  color: 'white'
}

// function myCall( color ){
//   document.body.style.background = this.background;
//   document.body.style.color = color;
// }
// myCall.call( colors, 'red' );

// function myCall() {
//   var createH1 = document.createElement('h1');
//   createH1.innerText = 'I know how binding works in JS';
//   document.body.appendChild(createH1);
//   document.body.style.color = this.color;
//   document.body.style.background = this.background;
// }

//1.1
// function myCall1(color) {
//   var createH1 = document.createElement('h1');
//   createH1.innerText = 'I know how binding works in JS';
//   document.body.appendChild(createH1);
//   document.body.style.color = color;
//   document.body.style.background = this.background;
// }
// myCall1.call(colors, 'yellow');

//1.2
// function myCall2() {
//   var createH1 = document.createElement('h1');
//   createH1.innerText = 'I know how binding works in JS';
//   document.body.appendChild(createH1);
//   document.body.style.color = this.color;
//   document.body.style.background = this.background;
// }
// var call2 = myCall2.bind(colors);
// call2();

//1.3
function myCall3(text) {
  var createH1 = document.createElement('h1');
  createH1.innerText = text;
  document.body.appendChild(createH1);
  document.body.style.color = this.color;
  document.body.style.background = this.background;
}
myCall3.apply(colors, ['2435623452']);